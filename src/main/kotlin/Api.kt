import com.google.gson.Gson
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import rx.Observable
import java.util.concurrent.TimeUnit

val mobileAggService: MobileAgg by lazy {
    makeRetrofit()
            .create(MobileAgg::class.java)
}

private fun makeRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl("https://api.myjson.com")//todo change to use gateway object or at the very least, change to https.
        .client(makeHttpClient())
        .addConverters()
        .build()

private fun Retrofit.Builder.addConverters(): Retrofit.Builder = this
        .addConverterFactory(GsonConverterFactory.create(Gson()))
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())


private fun makeHttpClient() = OkHttpClient.Builder()
        .connectTimeout(60, TimeUnit.SECONDS)
        .readTimeout(60, TimeUnit.SECONDS)
        .build()

interface MobileAgg {

    @GET("/bins/{binId}")
    fun getComparisonData(@Path("binId") binId: String) : Observable<Response<ComparisonResult>>

}