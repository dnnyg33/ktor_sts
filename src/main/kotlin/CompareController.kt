import com.google.gson.Gson
import rx.Observable

class CompareController {
    fun getCompareData(userRef: String): ComparisonResult? {
        return mobileAggService.getComparisonData(userRef)
                .flatMap { Observable.just(it.body()) }
                .toBlocking()
                .single()
    }

    private fun getJson(): String {
return """{
    "sampleId": "628C06FB-C093-4413-BE50-EAB3F74DD149",
    "comparableRegions": [
        {
            "sourceRegion": {
                "key": "AngloSaxon",
                "percentage": 0
            },
            "targetRegion": {
                "key": "AngloSaxon",
                "percentage": 48
            },
            "diff": "48"
        },
        {
            "sourceRegion": {
                "key": "Celtic",
                "percentage": 19
            },
            "targetRegion": {
                "key": "Celtic",
                "percentage": 19
            },
            "diff": "0"
        },
        {
            "sourceRegion": {
                "key": "EuropeW",
                "percentage": 36
            },
            "targetRegion": {
                "key": "France",
                "percentage": 13
            },
            "diff": "-23"
        },
        {
            "sourceRegion": {
                "key": "EuropeW",
                "percentage": 36
            },
            "targetRegion": {
                "key": "Germany",
                "percentage": 9
            },
            "diff": "-27"
        },
        {
            "sourceRegion": {
                "key": "EuropeS",
                "percentage": 15
            },
            "targetRegion": {
                "key": "Italy",
                "percentage": 4
            },
            "diff": "-11"
        },
        {
            "sourceRegion": {
                "key": "EuropeN",
                "percentage": 16
            },
            "targetRegion": {
                "key": "Sweden",
                "percentage": 3
            },
            "diff": "-13"
        },
        {
            "sourceRegion": {
                "key": "EuropeS",
                "percentage": 15
            },
            "targetRegion": {
                "key": "GreeceAlbania",
                "percentage": 2
            },
            "diff": "-13"
        },
        {
            "targetRegion": {
                "key": "Baltic",
                "percentage": 1
            },
            "diff": "1"
        },
        {
            "sourceRegion": {
                "key": "UralVolga",
                "percentage": 0
            },
            "targetRegion": {
                "key": "UralVolga",
                "percentage": 1
            },
            "diff": "1"
        }
    ],
    "vanishingRegions": [
        {
            "sourceRegion": {
                "key": "UralVolga",
                "percentage": 0
            },
            "targetRegion": {
                "key": "UralVolga",
                "percentage": 1
            },
            "diff": "1"
        }
    ]
}"""
        }

}


data class ComparisonResult(val sampleId: String, val comparableRegions: List<ComparableRegion>, val vanishingRegions: List<ComparableRegion>?) {

    data class ComparableRegion(val sourceRegion: Region?, val targetRegion: Region?, val diff: String) {
        data class Region(val key: String, val percentage: Float)
    }
}