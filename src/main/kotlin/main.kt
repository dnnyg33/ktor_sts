import io.ktor.application.*
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.text.DateFormat

fun main(args: Array<String>) {
    val server = embeddedServer(Netty, port = 8080) {

        //add plugin to application
        install(ContentNegotiation) {
            gson {
                setDateFormat(DateFormat.LONG)
                setPrettyPrinting()
            }
        }

        routing {
            get("/") {
                call.respondText("Hello World!", ContentType.Text.Plain)
            }
            get("/demo") {
                call.respondText("HELLO WORLD!")
            }
            get("/json") {
                val userRef = call.request.queryParameters["sampleId"] ?: throw IllegalArgumentException("FirebaseUID null")

                call.respond(CompareController().getCompareData(userRef)!!)
            }
        }
    }
    server.start(wait = true)
}

